INSTALLED_APPS = ['app']

WSGI_APPLICATION = 'orchestrator.wsgi.application'

ROOT_URLCONF = 'orchestrator.urls'

ALLOWED_HOSTS = ['*']

SECRET_KEY = 'secret.key'

DEBUG = True
