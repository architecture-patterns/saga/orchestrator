import os
import socketio

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'orchestrator.settings')

from app.sockets import sio

django_app = get_wsgi_application()
application = socketio.WSGIApp(sio, django_app)
