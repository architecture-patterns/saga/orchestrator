from django.http import HttpResponse
from django.urls import path


def start(request):
    return HttpResponse("Orchestrator server running...")


urlpatterns = [
    path('', start, name='home')
]
