import socketio


class UserNamespace(socketio.Namespace):
    @staticmethod
    def on_connect(sid, environ):
        print('User namespace connected')

    @staticmethod
    def on_disconnect(self, sid):
        print('User namespace disconnected')

    @staticmethod
    def on_register_user(self, sid, data):
        print('register user')
