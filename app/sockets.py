import socketio

from app.namespaces import UserNamespace

sio = socketio.Server(always_connect=True, cors_allowed_origins=[], logger=True)


@sio.event
def connect(sid, environment):
    print('connection established', sid)


@sio.event
def disconnect(sid):
    print('client disconnected', sid)


sio.register_namespace(UserNamespace('/user'))
